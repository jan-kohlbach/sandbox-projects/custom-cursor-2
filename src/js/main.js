const lerp = (a, b, n) => (1 - n) * a + n * b;

class Cursor {
  constructor() {
    this.element = document.getElementById('cursor');
    this.target = { x: 0.5, y: 0.5 };
    this.cursor = { x: 0.5, y: 0.5 };
    this.speed = 0.35;

    this.init();
  }

  bindAll() {
    // eslint-disable-next-line no-return-assign
    ['onmousemove', 'render'].forEach((fn) => (this[fn] = this[fn].bind(this)));
  }

  init() {
    this.bindAll();

    window.addEventListener('mousemove', this.onmousemove);
    this.raf = requestAnimationFrame(this.render);
  }

  onmousemove(e) {
    if (!this.element.classList.contains('active')) {
      this.element.classList.add('active');
    }

    this.target.x = e.clientX / window.innerWidth;
    this.target.y = e.clientY / window.innerHeight;

    if (!this.raf) this.raf = requestAnimationFrame(this.render);
  }

  render() {
    this.cursor.x = lerp(this.cursor.x, this.target.x, this.speed);
    this.cursor.y = lerp(this.cursor.y, this.target.y, this.speed);

    document.documentElement.style.setProperty('--cursor-x', this.cursor.x);
    document.documentElement.style.setProperty('--cursor-y', this.cursor.y);

    const delta = Math.sqrt(
      ((this.target.x - this.cursor.x) ** 2)
      + ((this.target.y - this.cursor.y) ** 2),
    );

    if (delta < 0.001) {
      cancelAnimationFrame(this.raf);
      this.raf = null;
      return;
    }

    this.raf = requestAnimationFrame(this.render);
  }
}

// eslint-disable-next-line no-new
new Cursor();
